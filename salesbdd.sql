create table categorie
(
    Nom varchar(16) not null
        primary key
)
    charset = utf8;

create table utilisateur
(
    NomComplet     varchar(64) not null,
    nomUtilisateur varchar(32) not null
        primary key,
    motDePasse     varchar(32) not null,
    Tel            varchar(16) not null
)
    charset = utf8;

create table annonce
(
    Id              int auto_increment
        primary key,
    Categorie       varchar(32)         not null,
    TypeTransaction varchar(32)         not null,
    Prix            decimal(10, 2)      not null,
    Description     text charset latin1 not null,
    Position        varchar(32)         not null,
    utilisateur     varchar(32)         not null,
    constraint annonce_categorie__fk
        foreign key (Categorie) references categorie (Nom),
    constraint annonce_utilisateur__fk
        foreign key (utilisateur) references utilisateur (nomUtilisateur)
)
    charset = utf8;

create table offre
(
    idoffre int auto_increment
        primary key,
    annonce int                              not null,
    sujet   varchar(45)                      not null,
    prix    double                           not null,
    etat    varchar(20) default 'En attente' not null,
    constraint fkutilisateur
        foreign key (sujet) references utilisateur (nomUtilisateur),
    constraint offre_annonce__fk
        foreign key (annonce) references annonce (Id)
)
    charset = utf8;

create index fkannonce_idx
    on offre (annonce);

create index fkutilisateur_idx
    on offre (sujet);



INSERT INTO categorie VALUES ('T1'),('T2'),('T3'),('T4');
INSERT INTO utilisateur VALUES ('Jean Patrick','JPleboss','incorrect','06 06 06 06 06'),('Julien Lepers','Lepers','motdepasse','06 12 34 56 78'),('test','test','test','0600000000');
INSERT INTO annonce VALUES (1,'T1','Location',255.00,'Pas le temps.','Tours','Lepers'),(2,'T2','Location',300.00,"C'est proposé par Julien lepers alors c'est forcément bien.",'Tours','Lepers'),(3,'T1','Location',254.99,"Hey, mec tu veux le meilleur appart de Tours, cherche pas ailleur, c'est le miens.",'Tours','JPleboss'),(44,'T2','Location',566.00,'EZ','Marseille','JPleboss'),(45,'T2','test',120.00,'test','test','test');