package DAO;


import Model.Annonce;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class AnnonceDAO {

    private ProcessingDB processingDB;

    public AnnonceDAO(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }


    public void ajoutAnnonce(Annonce annonce) throws SQLException {


        PreparedStatement preparedStatement = null;


        preparedStatement = processingDB.getConnection().prepareStatement("INSERT INTO annonce(Categorie, TypeTransaction, Prix, Description, Position, utilisateur) VALUES(?,?,?,?,?,?)");

        preparedStatement.setString(1, annonce.getCategorie());
        preparedStatement.setString(2, annonce.getTypeTransaction());
        preparedStatement.setFloat(3, annonce.getPrix());
        preparedStatement.setString(4, annonce.getDescription());
        preparedStatement.setString(5, annonce.getPosition());
        preparedStatement.setString(6, annonce.getUtilisateur());

        preparedStatement.executeUpdate();

        preparedStatement.close();

    }

    public List<Annonce> listerAnnonces() throws SQLException {
        ResultSet resultSet = null;

        ArrayList<Annonce> listAnnonces = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().executeQuery(
                "SELECT * FROM annonce"+
                " LEFT JOIN offre ON annonce.Id = offre.annonce"+
                " WHERE offre.etat IS NULL OR offre.etat <>'Acceptée' ;");

        while(resultSet.next()) {
            listAnnonces.add(new Annonce(
                    resultSet.getInt("Id"),
                    resultSet.getString("Categorie"),
                    resultSet.getString("TypeTransaction"),
                    resultSet.getFloat("Prix"),
                    resultSet.getString("Description"),
                    resultSet.getString("Position"),
                    resultSet.getString("utilisateur")));
        }

        return listAnnonces;

    }

    public List<Annonce> listerAnnoncesParUtilisateur(String utilisateur) throws SQLException {
        ResultSet resultSet = null;

        ArrayList<Annonce> listAnnonces = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().executeQuery("SELECT * FROM annonce where utilisateur="+"'"+utilisateur+"'");

        while(resultSet.next()) {
            listAnnonces.add(new Annonce(
                    resultSet.getInt("Id"),
                    resultSet.getString("Categorie"),
                    resultSet.getString("TypeTransaction"),
                    resultSet.getFloat("Prix"),
                    resultSet.getString("Description"),
                    resultSet.getString("Position"),
                    resultSet.getString("utilisateur")));
        }

        return listAnnonces;

    }



    public void suppressionAnnonce(int idAnnonce) throws SQLException {

        PreparedStatement preparedStatement = null;
        preparedStatement = processingDB.getConnection().prepareStatement("DELETE FROM annonce WHERE Id ="+idAnnonce);

        preparedStatement.executeUpdate();
        preparedStatement.close();

    }



    public List<String> listerCategories() throws SQLException {
        ResultSet resultSet = null;

        LinkedHashSet<String> hashset = new LinkedHashSet<String>();
        ArrayList<String> listeCategories = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().executeQuery("SELECT Categorie FROM annonce");

        while(resultSet.next()) {
            if(hashset.add(String.valueOf(resultSet.getString("Categorie")))) {
                listeCategories.add(resultSet.getString("Categorie"));
            }
        }

        return listeCategories;

    }



    public List<String> listerLocalisations() throws SQLException {
        ResultSet resultSet = null;

        LinkedHashSet<String> hashset = new LinkedHashSet<String>();
        ArrayList<String> listPosition = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().executeQuery("SELECT Position FROM annonce");

        while(resultSet.next()) {
            if(hashset.add(String.valueOf(resultSet.getString("Position")))) {
                listPosition.add(resultSet.getString("Position"));
            }
        }
        return listPosition;
    }


    public boolean verifExistenceAnnonce(Annonce annonce) throws SQLException {
        ResultSet resultSet = null;

        ArrayList<Annonce> listAnnonces = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().executeQuery(
                "SELECT * FROM annonce"+
                        " WHERE annonce.Categorie="+"'"+annonce.getCategorie()+"'"+
                        " AND annonce.TypeTransaction="+"'"+annonce.getTypeTransaction()+"'"+
                        " AND annonce.Prix="+"'"+annonce.getPrix()+"'"+
                        " AND annonce.Description="+"'"+annonce.getDescription()+"'"+
                        " AND annonce.Position="+"'"+annonce.getPosition()+"'"+
                        " AND annonce.utilisateur="+"'"+annonce.getUtilisateur()+"'"+";");


        return resultSet.next();
    }







}
