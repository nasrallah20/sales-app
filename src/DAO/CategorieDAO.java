package DAO;

import Model.Categorie;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CategorieDAO {

    private ProcessingDB processingDB;


    public CategorieDAO(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }


    public void ajoutCategorie(Categorie categorie) throws SQLException {

        PreparedStatement preparedStatement = null;


        preparedStatement = processingDB.getConnection().prepareStatement("INSERT INTO categorie VALUES(?)");

        preparedStatement.setString(1, categorie.getName());

        preparedStatement.executeUpdate();

        preparedStatement.close();



    }

    public boolean selectionCategorie(String nom) throws SQLException {

        ResultSet resultset = null;

        resultset = processingDB.getConnection().createStatement().executeQuery("SELECT * from categorie where Nom="+"'"+nom+"'");

        return resultset.next();
    }



}
