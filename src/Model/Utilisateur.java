package Model;

public class Utilisateur {
    private String fullName;
    private String nomUtilisateur;
    private String motDePasse;
    private String phoneNumber;


    public Utilisateur() {
    }

    public Utilisateur(String fullName, String nomUtilisateur, String motDePasse, String phoneNumber) {
        this.fullName = fullName;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.phoneNumber = phoneNumber;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getnomUtilisateur() {
        return nomUtilisateur;
    }

    public void setnomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getmotDePasse() {
        return motDePasse;
    }

    public void setmotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
