package Model;

public class Offre {

    private int idAnnonce;
    private String utilisateur;
    private String sujet;
    private float prix;
    private String etat;


    public Offre() {

    }


    //Constructeur pour récuperer le publicateur de l'annonce ou le générateur de l'offre
    public Offre(String utilisateur, float prix, String etat) {
        this.utilisateur = utilisateur;
        this.sujet = sujet;
        this.prix = prix;
        this.etat = etat;
    }

    public Offre(int idAnnonce, String sujet, float prix, String etat) {
        this.idAnnonce=idAnnonce;
        this.sujet = sujet;
        this.prix = prix;
        this.etat = etat;
    }

    public Offre(int idAnnonce, String utilisateur, String sujet, float prix, String etat) {
        this.idAnnonce=idAnnonce;
        this.utilisateur=utilisateur;
        this.sujet = sujet;
        this.prix = prix;
        this.etat = etat;

    }


    public int getIdAnnonce() {
        return idAnnonce;
    }

    public void setIdAnnonce(int idAnnonce) {
        this.idAnnonce = idAnnonce;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }
}
