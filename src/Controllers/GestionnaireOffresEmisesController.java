package Controllers;

import DAO.OffreDAO;
import DAO.ProcessingDB;
import Model.Annonce;
import Model.Offre;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.SQLException;
import java.util.Iterator;

public class GestionnaireOffresEmisesController {

    private OffreDAO offreDAO;
    private Annonce annonceCible;

    public Annonce getAnnonceCible() {
        return annonceCible;
    }

    public void setAnnonceCible(Annonce annonceCible) {
        this.annonceCible = annonceCible;
    }

    @FXML
    private Label labelnomUtilisateur;

    public Label getLabelnomUtilisateur() {
        return labelnomUtilisateur;
    }

    @FXML
    private TableView<Offre> offresTBV;

    @FXML
    private TableColumn<Offre, String> stateCol;

    @FXML
    private TableColumn<Offre, String> userCol;

    @FXML
    private TableColumn<Offre, Float> priceCol;


    public GestionnaireOffresEmisesController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.offreDAO = processingDB.getOffreDAO();
    }


    public void initialize() {




    }

    public void remplissage() throws SQLException {

        ObservableList<Offre> observableListOffres = FXCollections.observableArrayList(offreDAO.listerOffresEmises(labelnomUtilisateur.getText()));

        userCol.setCellValueFactory(new PropertyValueFactory<>("utilisateur"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("Prix"));
        stateCol.setCellValueFactory(new PropertyValueFactory<>("Etat"));


        offresTBV.setItems(observableListOffres);
    }


}
