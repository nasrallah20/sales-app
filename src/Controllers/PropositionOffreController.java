package Controllers;

import DAO.OffreDAO;
import DAO.ProcessingDB;
import Model.Annonce;
import Model.Offre;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;

public class PropositionOffreController {

    private OffreDAO offreDAO;

    private Annonce annonceCible;

    public Annonce getAnnonceCible() {
        return annonceCible;
    }

    public void setAnnonceCible(Annonce annonceCible) {
        this.annonceCible = annonceCible;
    }

    private String utilisateurCible;

    public String getUtilisateurCible() {
        return utilisateurCible;
    }

    public void setUtilisateurCible(String utilisateurCible) {
        this.utilisateurCible = utilisateurCible;
    }



    @FXML // fx:id="textPrixOffre"
    private TextField textPrixOffre; // Value injected by FXMLLoader

    @FXML // fx:id="ajoutOffreBtn"
    private Button ajoutOffreBtn; // Value injected by FXMLLoader

    @FXML // fx:id="labelAnnonceOffre"
    private Label labelUtilisateurCible; // Value injected by FXMLLoader

    public TextField getTextPrixOffre() {
        return textPrixOffre;
    }

    public void setTextPrixOffre(TextField textPrixOffre) {
        this.textPrixOffre = textPrixOffre;
    }

    public Label getlabelUtilisateurCible() {
        return labelUtilisateurCible;
    }

    public void setLabelAnnonceOffre(Label labelAnnonceOffre) {
        this.labelUtilisateurCible = labelAnnonceOffre;
    }


    public PropositionOffreController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.offreDAO = processingDB.getOffreDAO();
    }

    public void initialize() {
        labelUtilisateurCible.setContentDisplay(ContentDisplay.CENTER);
    }

    @FXML
    void addOffre(ActionEvent event) throws SQLException {

        Offre offre = new Offre();
        offre.setIdAnnonce(annonceCible.getId());
        offre.setSujet(utilisateurCible);
        offre.setPrix(Float.parseFloat(textPrixOffre.getText()));

        offreDAO.ajoutOffre(offre);

        //On récupère la fenêtre en cours
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

    }




}
