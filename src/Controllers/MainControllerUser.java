package Controllers;

import DAO.AnnonceDAO;
import DAO.ProcessingDB;
import Model.Annonce;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.IOException;
import java.sql.SQLException;
import java.util.function.Predicate;

public class MainControllerUser {

    private ObservableList<Annonce> donnees;
    private AnnonceDAO annonceDAO;



    public ObservableList<Annonce> getDonnees() {
        return donnees;
    }

    public void setDonnees(ObservableList<Annonce> donnees) {
        this.donnees = donnees;
    }

    @FXML // fx:id="annoncesManageBtn"
    private Button annoncesManageBtn; // Value injected by FXMLLoader

    @FXML // fx:id="annoncesTBV"
    private TableView<Annonce> annoncesTBV; // Value injected by FXMLLoader

    public TableView<Annonce> getAnnoncesTBV() {
        return annoncesTBV;
    }

    @FXML // fx:id="columnType"
    private TableColumn<Annonce, String> columnType; // Value injected by FXMLLoader


    @FXML // fx:id="columnUser"
    private TableColumn<Annonce, String> columnUser; // Value injected by FXMLLoader

    @FXML // fx:id="columnPos"
    private TableColumn<Annonce, String> columnPos; // Value injected by FXMLLoader

    @FXML // fx:id="columnCat"
    private TableColumn<Annonce, String> columnCat; // Value injected by FXMLLoader

    @FXML // fx:id="columnPrice"
    private TableColumn<Annonce, Float> columnPrice; // Value injected by FXMLLoader

    @FXML // fx:id="columnDesc"
    private TableColumn<Annonce, String> columnDesc; // Value injected by FXMLLoader

    @FXML
    private Label labelnomUtilisateur;
    public Label getLabelnomUtilisateur() {
        return labelnomUtilisateur;
    }

    @FXML
    private Button offreManagerBtn;

    @FXML
    private ComboBox<String> categoryCBX;
    @FXML
    private ComboBox<String> positionCBX;


    @FXML
    private Button refreshBtn;

    @FXML
    private Button filtresOff;

    @FXML
    private Button logOut;

    @FXML
    private Button offresEmisesBtn;



    public MainControllerUser() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.annonceDAO = processingDB.getAnnonceDAO();


    }


    public void initialize() throws IOException, SQLException {

        setDonnees(FXCollections.observableArrayList(annonceDAO.listerAnnonces()));

        columnUser.setCellValueFactory(new PropertyValueFactory<>("Utilisateur"));
        columnPos.setCellValueFactory(new PropertyValueFactory<>("Position"));
        columnType.setCellValueFactory(new PropertyValueFactory<>("TypeTransaction"));
        columnCat.setCellValueFactory(new PropertyValueFactory<>("Categorie"));
        columnPrice.setCellValueFactory(new PropertyValueFactory<>("Prix"));
        columnDesc.setCellValueFactory(new PropertyValueFactory<>("Description"));

        FilteredList<Annonce> filterData = new FilteredList<>(donnees);
        SortedList<Annonce> personSortedList = new SortedList<>(filterData);
        annoncesTBV.setItems(personSortedList);

        personSortedList.comparatorProperty().bind(annoncesTBV.comparatorProperty());

        //Filtrage grâce au combobox

        //Filtre des catégories
        ObservableList<String> filterCategories = FXCollections.observableArrayList(annonceDAO.listerCategories());
        categoryCBX.getItems().addAll(filterCategories);

        ObservableList<String> filterLocalisations = FXCollections.observableArrayList(annonceDAO.listerLocalisations());
        positionCBX.getItems().addAll(filterLocalisations);

        ObjectProperty<Predicate<Annonce>> catFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Annonce>> locFilter = new SimpleObjectProperty<>();

        //On lie les deux filtres
        catFilter.bind(Bindings.createObjectBinding(() ->
                        annonce -> categoryCBX.getValue() == null || categoryCBX.getValue().equals(annonce.getCategorie()),
                        categoryCBX.valueProperty()));
        locFilter.bind(Bindings.createObjectBinding(() ->
                        annonce -> positionCBX.getValue() == null || positionCBX.getValue().equals(annonce.getPosition()),
                        positionCBX.valueProperty()));


        //On applique les deux filtres à la table
        filterData.predicateProperty().bind(Bindings.createObjectBinding(
                () -> catFilter.get().and(locFilter.get()),
                catFilter, locFilter));



        filtresOff.setOnAction(e -> {
            positionCBX.setValue(null);
            categoryCBX.setValue(null);
        });




        annoncesTBV.setRowFactory( tv -> {
            TableRow<Annonce> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                //On récupère la ligne selectionnée
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    int index = annoncesTBV.getSelectionModel().getSelectedIndex();
                    Annonce annonceCible = annoncesTBV.getItems().get(index);

                    //Si on clique sur l'annonce de l'utilisateur en cours, on renvoie une erreur
                    if(!labelnomUtilisateur.getText().equals(annonceCible.getUtilisateur())) {
                        Float prix = annonceCible.getPrix();
                        String nomUtilisateur = annoncesTBV.getItems().get(index).getUtilisateur();

                        Stage stage = new Stage();
                        try {

                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/PropositionOffre.fxml"));
                            Region root = loader.load();

                            Scene scene = new Scene(root);
                            //On récupère ...
                            PropositionOffreController offreController = loader.<PropositionOffreController>getController();
                            offreController.setUtilisateurCible(labelnomUtilisateur.getText());
                            offreController.setAnnonceCible(annonceCible);
                            offreController.getlabelUtilisateurCible().setText(String.valueOf(nomUtilisateur));
                            offreController.getTextPrixOffre().setText(String.valueOf(prix));

                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.setTitle("Proposer une offre");
                            stage.setResizable(false);
                            stage.show();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                    else {
                        allertDialog("Erreur !", Alert.AlertType.ERROR, "Vous avez selectionné votre annonce !");

                    }

                }
            });
            return row ;
        });





    }

    @FXML
    void manageAnnonces(ActionEvent event) throws IOException, SQLException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/GestionAnnonces.fxml"));
        Region root = loader.load();

        GestionAnnonceController gestionAnnonceController = loader.<GestionAnnonceController>getController();
        gestionAnnonceController.setMainControllerUser(this);
        //On récupère le nomUtilisateur pour l'ajouter en cas de création d'une annonce par l'utilisateur
        gestionAnnonceController.getGAlabelnomUtilisateur().setText(labelnomUtilisateur.getText());
        gestionAnnonceController.getnomUtilisateurField().setText(labelnomUtilisateur.getText());
        gestionAnnonceController.remplissage();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);


        st.setResizable(false);
        st.setTitle("Gestion de mes annonces");
        st.show();


    }


    @FXML
    void refreshTable(ActionEvent event) throws SQLException {
        annoncesTBV.refresh();
    }



    @FXML
    void logOut(ActionEvent event) throws IOException {

        //On récupère la fenêtre de login
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

        //On redirige vers la fenêtre d'authentification
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Authentification.fxml"));

        Region root = loader.load();
        Scene scene = new Scene(root);
        st.setScene(scene);
        st.setTitle("Authentification");
        st.setResizable(false);
        st.show();


    }

    @FXML
    void gestionOffresRecues(ActionEvent event) throws IOException, SQLException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/GestionnaireOffresRecues.fxml"));
        Region root = loader.load();

        GestionnaireOffresRecuesController gestionnaireOffresRecuesController = loader.<GestionnaireOffresRecuesController>getController();

        //On récupère le nomUtilisateur pour l'ajouter dans le label
        gestionnaireOffresRecuesController.getLabelnomUtilisateur().setText(labelnomUtilisateur.getText());
        gestionnaireOffresRecuesController.setMainControllerUser(this);
        gestionnaireOffresRecuesController.remplissage();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Offres reçues");
        st.show();

    }

    @FXML
    void gestionOffresEmises(ActionEvent event) throws IOException, SQLException {


        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/GestionnaireOffresEmises.fxml"));
        Region root = loader.load();

        GestionnaireOffresEmisesController gestionnaireOffresEmisesController = loader.<GestionnaireOffresEmisesController>getController();

        //On récupère le nomUtilisateur pour l'ajouter dans le label
        gestionnaireOffresEmisesController.getLabelnomUtilisateur().setText(labelnomUtilisateur.getText());
        //On passe l'annonce récupérée au controlleur de gestion d'offres émises
        //gestionnaireOffresEmisesController.setAnnonceCible(annonceDAO.);
        gestionnaireOffresEmisesController.remplissage();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Offres émises");
        st.show();



    }


    //Méthode gérant les alertes
    private void allertDialog(String header,Alert.AlertType alertType, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Dialog info");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }











}
