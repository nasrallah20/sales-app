package Controllers;

import DAO.AnnonceDAO;
import DAO.ProcessingDB;
import Model.Annonce;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.function.Predicate;

public class MainControllerVisitor {

    private ObservableList<Annonce> donnees;
    private AnnonceDAO annonceDAO;

    public void setDonnees(ObservableList<Annonce> donnees) {
        this.donnees = donnees;
    }

    public MainControllerVisitor() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.annonceDAO = processingDB.getAnnonceDAO();

    }

    @FXML
    private Label labelnomUtilisateur1;

    @FXML
    private TableColumn<Annonce, String> columnUser;

    @FXML
    private Button filtresOff;

    @FXML
    private TableColumn<Annonce, String> columnPos;

    @FXML
    private Button refreshBtn;

    @FXML
    private TableColumn<Annonce, String> columnType;

    @FXML
    private ComboBox<String> categoryCBX;

    @FXML
    private TableColumn<Annonce, String> columnCat;

    @FXML
    private TableView<Annonce> annoncesTBV;

    @FXML
    private TableColumn<Annonce, Float> columnPrice;

    @FXML
    private ComboBox<String> positionCBX;

    @FXML
    private Button logInBtn;

    @FXML
    private TableColumn<?, ?> columnDesc;



    public void initialize() throws IOException, SQLException {

        setDonnees(FXCollections.observableArrayList(annonceDAO.listerAnnonces()));

        columnUser.setCellValueFactory(new PropertyValueFactory<>("Utilisateur"));
        columnPos.setCellValueFactory(new PropertyValueFactory<>("Position"));
        columnType.setCellValueFactory(new PropertyValueFactory<>("TypeTransaction"));
        columnCat.setCellValueFactory(new PropertyValueFactory<>("Categorie"));
        columnPrice.setCellValueFactory(new PropertyValueFactory<>("Prix"));
        columnDesc.setCellValueFactory(new PropertyValueFactory<>("Description"));

        FilteredList<Annonce> filterData = new FilteredList<>(donnees);
        SortedList<Annonce> personSortedList = new SortedList<>(filterData);
        annoncesTBV.setItems(personSortedList);

        personSortedList.comparatorProperty().bind(annoncesTBV.comparatorProperty());

        //Filtrage grâce au combobox

        //Filtre des catégories
        ObservableList<String> filterCategories = FXCollections.observableArrayList(annonceDAO.listerCategories());
        categoryCBX.getItems().addAll(filterCategories);

        ObservableList<String> filterLocalisations = FXCollections.observableArrayList(annonceDAO.listerLocalisations());
        positionCBX.getItems().addAll(filterLocalisations);

        ObjectProperty<Predicate<Annonce>> catFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Annonce>> locFilter = new SimpleObjectProperty<>();

        //On lie les deux filtres
        catFilter.bind(Bindings.createObjectBinding(() ->
                        annonce -> categoryCBX.getValue() == null || categoryCBX.getValue().equals(annonce.getCategorie()),
                categoryCBX.valueProperty()));
        locFilter.bind(Bindings.createObjectBinding(() ->
                        annonce -> positionCBX.getValue() == null || positionCBX.getValue().equals(annonce.getPosition()),
                positionCBX.valueProperty()));


        //On applique les deux filtres à la table
        filterData.predicateProperty().bind(Bindings.createObjectBinding(
                () -> catFilter.get().and(locFilter.get()),
                catFilter, locFilter));

        filtresOff.setOnAction(e -> {
            positionCBX.setValue(null);
            categoryCBX.setValue(null);
        });

    }


    @FXML
    void logIn(ActionEvent event) throws IOException {

        //On récupère la fenêtre actuelle
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

        //On redirige vers la fenêtre d'authentification
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Authentification.fxml"));
        Region root = loader.load();

        Scene scene = new Scene(root);
        st.setScene(scene);

        st.setResizable(false);
        st.show();


    }
}
