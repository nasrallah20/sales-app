package Controllers;

import DAO.ProcessingDB;
import DAO.UtilisateurDAO;
import Model.Utilisateur;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class InscriptionController {

    private UtilisateurDAO utilisateurDAO;

    public UtilisateurDAO getUtilisateurDAO() {
        return utilisateurDAO;
    }

    @FXML
    private TextField nomUtilisateurSUInput;

    @FXML
    private TextField fullNameInput;

    @FXML
    private PasswordField pswdSUInput;

    @FXML
    private TextField phoneNbnput;

    @FXML
    private Button signUp;




    public InscriptionController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.utilisateurDAO = processingDB.getUtilisateurDAO();
    }


    public void initialize() {

    }


    @FXML
    void signUp(ActionEvent event) throws SQLException {

        if(utilisateurDAO.verifnomUtilisateur(nomUtilisateurSUInput.getText())) {
            allertDialog("Erreur", Alert.AlertType.ERROR, "Utilisateur déjà inscrit !");
        }
        else {
            //Si numéro de téléphone invalide
            if(!phoneNbnput.getText().matches("^" +
                    "    (?:(?:\\+|00)33|0)     # Code pays\n" +
                    "    \\s*[1-9]              # Premier chiffre (De 1 à 9)" +
                    "    (?:[\\s.-]*\\d{2}){4}   # Fin du numéro de téléphone" +
                    "$"))
            {
                allertDialog("Erreur !", Alert.AlertType.ERROR, "Merci d'entrer un numéro de téléphone valide !");
            }

            else {
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setFullName(fullNameInput.getText());
                utilisateur.setmotDePasse(pswdSUInput.getText());
                utilisateur.setPhoneNumber(phoneNbnput.getText());
                utilisateur.setnomUtilisateur(nomUtilisateurSUInput.getText());
                utilisateurDAO.inscription(utilisateur);

            }

            allertDialog("Requête exécutée !", Alert.AlertType.ERROR, "Inscription réussie !");

        }


    }


    //Méthode gérant les alertes
    private void allertDialog(String header,Alert.AlertType alertType, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Dialog info");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }

}
